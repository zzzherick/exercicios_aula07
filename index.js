const express = require('express')
const app = express()
const fs = require('fs')

var produtos = []

app.use(express.urlencoded())

app.use("/unesc", function(req, res, next){
    console.log("Acesso realizado "+ Date.now())
    next()
})

app.use(express.static("documentos"))

app.post("/unesc", (req, res) => {
    req.body.senha = "****"
    res.send(req.body)
})

app.post("/produtos", (req, res) => {
    var produto = {
        "nome": req.body.nome,
        "preco": req.body.preco
    }
    
    produtos.push(produto)

    nome_arquivo = "produtos.txt"
    conteudo = JSON.stringify(produto)

    var option = {
        encoding: "utf8",
        flag: "a"
    }

    var arquivo = fs.writeFile(nome_arquivo, conteudo, option, (err) => {
        if(err){
            console.error(err)
        } else{
            console.log(conteudo + " gravado em "+ nome_arquivo)
        }
    })

    res.send(produtos)
})

app.get("/produtos", (req, res, next) => {
    var arquivo = fs.readFile(nome_arquivo)

    res.send(arquivo)
})

/*app.get("/unesc", (res, req) => {
    res.redirect("http://unesc.net/")
})
*/


app.get("/", (req, res) => {
    console.log("Requisição GET")
    console.log(req.ip)

    //http://localhost:8080/?q=unes&nome=herick
    //res.send(req.query.nome)

    //http://localhost:8080/herick
    //res.send(req.params)

    //res.redirect("https://unesc.net")

    //res.send("Tudo OK!")
})

app.listen(8080, () => {
    console.log("O servidor está online")
})